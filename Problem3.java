import java.util.Scanner;
public class Problem3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number = ");
        int num1 = sc.nextInt();
        System.out.print("Please input  second number = ");
        int num2 = sc.nextInt();

        int i = 0 ;

        if( num1 <= num2){
            i = num1;
            while( i <= num2){
                System.out.print(i+" ");
                i++;
            }
        }
        else{
            System.out.println("Error");
        }
    }
}
