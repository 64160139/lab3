import java.util.Scanner;
public class Problem18new {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

            int num ;
        do {
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            num = sc.nextInt();
            if(num == 1){
                System.out.print("Please input number: ");
                int one = sc.nextInt();
                for(int i = 0 ; i < one ; i++){
                    for(int j = 0 ; j < one ; j++){
                        if(j<=i){
                            System.out.print("*");
                        }
                    }
                    System.out.println();
                }
            }
            else if(num == 2){
                System.out.print("Please input number: ");
                int two = sc.nextInt();
                for(int i = 0 ; i < two ; i++){
                    for(int j = 0 ; j < two ; j++){
                        if(j>=i){
                            System.out.print("*");
                        }
                    }
                    System.out.println();
                }
            }
            else if(num == 3){
                System.out.print("Please input number: ");
                int three = sc.nextInt();
                for(int i =0 ; i < three ; i++){
                    for(int j = 0 ; j < three ; j++){
                        if(i > j){
                            System.out.print(" ");
                        }
                        else{
                            System.out.print("*");
                        }
                    }
                    System.out.println();
                }
            }
            else if(num == 4 ){
                System.out.print("Please input number: ");
                int four = sc.nextInt();
                for(int i = 0 ; i < four ; i++){
                    for(int j = 0 ; j < four ; j++){
                        if((i+j) >3 ){
                            System.out.print("*");
                        }
                        else{
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }
            else{
                System.out.print("Error: Please input number between 1-5");
            }
        } while (num != 5);
        System.out.print("Bye bye!!!");
    }
}
